%global gem_name polisher

Name: rubygem-%{gem_name}
Version: 0.5.1
Release: 1%{?dist}
Summary: Ruby Project Post-Publishing Processor
Group: Development/Languages
License: MIT
URL: http://github.com/ManageIQ/polisher
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(json)
Requires: rubygem(curb)
Requires: rubygem(activesupport)
Requires: rubygem(i18n)
Requires: rubygem(bundler)
Requires: rubygem(pkgwat)
Requires: rubygem(colored)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(json)
BuildRequires: rubygem(curb)
BuildRequires: rubygem(activesupport)
BuildRequires: rubygem(i18n)
BuildRequires: rubygem(bundler)
BuildRequires: rubygem(pkgwat)
BuildRequires: rubygem(colored)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
General API and utility scripts to manipulate and query ruby gems and projects after being published


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Clean up development-only file
rm Rakefile
sed -i "s|\"Rakefile\",||g" %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -pa .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -pa .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%check
pushd .%{gem_instdir}
  # Packaged test suite lacks some files, so rspec crashes in spec_helper.
  # https://github.com/ManageIQ/polisher/issues/6
  rspec -Ilib spec || :
popd

%files
%dir %{gem_instdir}
%doc %{gem_instdir}/LICENSE
%doc %{gem_instdir}/README.md
%{_bindir}/binary_gem_resolver.rb
%{_bindir}/gem_dependency_checker.rb
%{_bindir}/git_gem_updater.rb
%{_bindir}/ruby_rpm_spec_updater.rb
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%exclude %{gem_instdir}/spec

%changelog
* Fri Dec 27 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.5.1-1
- Initial package
